package com.example.aep_project.dyslexiaapplication;
import android.app.Instrumentation;
import android.content.Intent;
import android.test.TouchUtils;
import android.widget.Button;
/**
 * Created by AEP-Project on 12/9/2014.
 */
public class gameActivityTest extends android.test.ActivityUnitTestCase<gameActivity> {
    public gameActivityTest() {
        super(gameActivity.class);
    }
    private int buttonId;
    private gameActivity activity;
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext() , gameActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }
    public void testLayout() {
        buttonId = R.id.game1;
        assertNotNull(activity.findViewById(buttonId));
        Button view = (Button) activity.findViewById(buttonId);
        assertEquals("Incorrect label of the button", "puzzle", view.getText());
    }
    public void testIntentTriggerViaOnClick() {
        buttonId = R.id.game1;
        Button view = (Button) activity.findViewById(buttonId);
        assertNotNull("Button not allowed to be null", view);
        view.performClick();
        // TouchUtils cannot be used, only allowed in
        // InstrumentationTestCase or ActivityInstrumentationTestCase2
        // Check the intent which was started
        Intent triggeredIntent = getStartedActivityIntent();
        assertNotNull("Intent was null", triggeredIntent);
    }
}
