package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;


public class puzzleLevel2Activity extends Activity implements View.OnClickListener{

    ImageView section1, section2, section3, section4, section5, section6;
    ImageView frame1, frame2, frame3, frame4, frame5, frame6;
    ImageView stars;
    ImageView nextLevel;
    ImageView menu ;
    ImageView feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_level2);

        section1  = (ImageView)findViewById(R.id.section1);
        section2  = (ImageView)findViewById(R.id.section2);
        section3  = (ImageView)findViewById(R.id.section3);
        section4  = (ImageView)findViewById(R.id.section4);
        section5  = (ImageView)findViewById(R.id.section5);
        section6  = (ImageView)findViewById(R.id.section6);
        stars     = (ImageView)findViewById(R.id.stars);
        nextLevel = (ImageView)findViewById(R.id.nextLevel);
        menu      = (ImageView)findViewById(R.id.menu);
        feedback =(ImageView)findViewById(R.id.feedback);

        ChoiceTouchListener choice = new ChoiceTouchListener();

        section1.setOnTouchListener(choice);
        section2.setOnTouchListener(choice);
        section3.setOnTouchListener(choice);
        section4.setOnTouchListener(choice);
        section5.setOnTouchListener(choice);
        section6.setOnTouchListener(choice);
        nextLevel.setOnClickListener(this);
        menu.setOnClickListener(this);

        frame1 = (ImageView)findViewById(R.id.frame1);
        frame2 = (ImageView)findViewById(R.id.frame2);
        frame3 = (ImageView)findViewById(R.id.frame3);
        frame4 = (ImageView)findViewById(R.id.frame4);
        frame5 = (ImageView)findViewById(R.id.frame5);
        frame6 = (ImageView)findViewById(R.id.frame6);

        ChoiceDragListener drag = new ChoiceDragListener();

        frame1.setOnDragListener(drag);
        frame2.setOnDragListener(drag);
        frame3.setOnDragListener(drag);
        frame4.setOnDragListener(drag);
        frame5.setOnDragListener(drag);
        frame6.setOnDragListener(drag);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.puzzle_level2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.nextLevel){
            Intent intent = new Intent(puzzleLevel2Activity.this, puzzleLevel3Activity.class);
            startActivity(intent);
            finish();
        }
        if(v.getId()==R.id.menu){
            Intent intentM = new Intent (puzzleLevel2Activity.this, gameActivity.class);
            startActivity(intentM);
            finish();
        }
    }

    private class ChoiceTouchListener implements View.OnTouchListener  {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //setup drag

                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);

                //start dragging the item touched
                v.startDrag(data, shadowBuilder, v, 0);
                return true;
            }
            else {
                return false;
            }
        }
    }

    private class ChoiceDragListener implements View.OnDragListener {
        int count = 0;
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {

            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:

                    View view = (View) dragEvent.getLocalState();
                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;
                    //view being dragged and dropped
                    ImageView dropped = (ImageView) view;

                    if ((dropped.getId()== R.id.section1)&&(dropTarget.getId()==R.id.frame1)) {
                        dropped.setVisibility(View.INVISIBLE);
                        frame1.setImageResource(R.drawable.section1);
                        count++;
                    }
                    else {
                        if ((dropped.getId() == R.id.section2) && (dropTarget.getId() == R.id.frame2)) {
                            dropped.setVisibility(View.INVISIBLE);
                            frame2.setImageResource(R.drawable.section2);
                            count++;
                        }
                        else {
                            if ((dropped.getId() == R.id.section3) && (dropTarget.getId() == R.id.frame3)) {
                                dropped.setVisibility(View.INVISIBLE);
                                frame3.setImageResource(R.drawable.section3);
                                count++;
                            }
                            else {
                                if ((dropped.getId() == R.id.section4) && (dropTarget.getId() == R.id.frame4)) {
                                    dropped.setVisibility(View.INVISIBLE);
                                    frame4.setImageResource(R.drawable.section4);
                                    count++;
                                }
                                else {
                                    if ((dropped.getId() == R.id.section5) && (dropTarget.getId() == R.id.frame5)) {
                                        dropped.setVisibility(View.INVISIBLE);
                                        frame5.setImageResource(R.drawable.section5);
                                        count++;
                                    }
                                    else {
                                        if ((dropped.getId() == R.id.section6) && (dropTarget.getId() == R.id.frame6)) {
                                            dropped.setVisibility(View.INVISIBLE);
                                            frame6.setImageResource(R.drawable.section6);
                                            count++;
                                        }
                                        else
                                        {
                                            feedback.setImageResource(R.drawable.cross);
                                            feedback.setVisibility(View.VISIBLE);
                                            feedback.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    feedback.setVisibility(View.INVISIBLE);
                                                }
                                            }, 1500);
                                        }

                                    }
                                }
                            }
                        }
                    }
                    if (count==6){
                        stars.setVisibility(View.VISIBLE);
                        nextLevel.setVisibility(View.VISIBLE);
                    }
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }


    }

    public void onBackPressed() {
        Intent intent = new Intent(puzzleLevel2Activity.this , puzzleLevel1Activity.class);
        startActivity(intent);
        finish();
    }
}

