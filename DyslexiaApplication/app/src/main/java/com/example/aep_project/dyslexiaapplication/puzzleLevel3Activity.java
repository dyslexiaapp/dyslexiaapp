package com.example.aep_project.dyslexiaapplication;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;



public class puzzleLevel3Activity extends ActionBarActivity implements View.OnClickListener {

    ImageView piece1, piece2, piece3, piece4, piece5, piece6, piece7, piece8, piece9;
    ImageView frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8, frame9;
    ImageView stars;
    ImageView menu;
    ImageView feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_level3);

        piece1 = (ImageView)findViewById(R.id.piece1);
        piece2 = (ImageView)findViewById(R.id.piece2);
        piece3 = (ImageView)findViewById(R.id.piece3);
        piece4 = (ImageView)findViewById(R.id.piece4);
        piece5 = (ImageView)findViewById(R.id.piece5);
        piece6 = (ImageView)findViewById(R.id.piece6);
        piece7 = (ImageView)findViewById(R.id.piece7);
        piece8 = (ImageView)findViewById(R.id.piece8);
        piece9 = (ImageView)findViewById(R.id.piece9);
        stars  = (ImageView)findViewById(R.id.stars);
        menu   = (ImageView) findViewById(R.id.menu);
        feedback =(ImageView)findViewById(R.id.feedback);

        ChoiceTouchListener choice = new ChoiceTouchListener();

        piece1.setOnTouchListener(choice);
        piece2.setOnTouchListener(choice);
        piece3.setOnTouchListener(choice);
        piece4.setOnTouchListener(choice);
        piece5.setOnTouchListener(choice);
        piece6.setOnTouchListener(choice);
        piece7.setOnTouchListener(choice);
        piece8.setOnTouchListener(choice);
        piece9.setOnTouchListener(choice);
        menu.setOnClickListener(this);

        frame1 = (ImageView)findViewById(R.id.frame1);
        frame2 = (ImageView)findViewById(R.id.frame2);
        frame3 = (ImageView)findViewById(R.id.frame3);
        frame4 = (ImageView)findViewById(R.id.frame4);
        frame5 = (ImageView)findViewById(R.id.frame5);
        frame6 = (ImageView)findViewById(R.id.frame6);
        frame7 = (ImageView)findViewById(R.id.frame7);
        frame8 = (ImageView)findViewById(R.id.frame8);
        frame9 = (ImageView)findViewById(R.id.frame9);

        ChoiceDragListener drag = new ChoiceDragListener();

        frame1.setOnDragListener(drag);
        frame2.setOnDragListener(drag);
        frame3.setOnDragListener(drag);
        frame4.setOnDragListener(drag);
        frame5.setOnDragListener(drag);
        frame6.setOnDragListener(drag);
        frame7.setOnDragListener(drag);
        frame8.setOnDragListener(drag);
        frame9.setOnDragListener(drag);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.puzzle_level3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.menu){
            Intent intent = new Intent(puzzleLevel3Activity.this, gameActivity.class);
            startActivity(intent);
            finish();
        }
    }


    private class ChoiceTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //setup drag

                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);

                //start dragging the item touched
                v.startDrag(data, shadowBuilder, v, 0);
                return true;
            }
            else {
                return false;
            }
        }
    }

    private class ChoiceDragListener implements View.OnDragListener {
        int count = 0;
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {

            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:

                    View view = (View) dragEvent.getLocalState();
                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;
                    //view being dragged and dropped
                    ImageView dropped = (ImageView) view;


                    if ((dropped.getId()== R.id.piece1)&&(dropTarget.getId()==R.id.frame1)) {
                        dropped.setVisibility(View.INVISIBLE);
                        frame1.setImageResource(R.drawable.piece1);
                        count++;
                    }
                    else {
                        if ((dropped.getId() == R.id.piece2) && (dropTarget.getId() == R.id.frame2)) {
                            dropped.setVisibility(View.INVISIBLE);
                            frame2.setImageResource(R.drawable.piece2);
                            count++;
                        }
                        else {
                            if ((dropped.getId() == R.id.piece3) && (dropTarget.getId() == R.id.frame3)) {
                                dropped.setVisibility(View.INVISIBLE);
                                frame3.setImageResource(R.drawable.piece3);
                                count++;
                            } else {
                                if ((dropped.getId() == R.id.piece4) && (dropTarget.getId() == R.id.frame4)) {
                                    dropped.setVisibility(View.INVISIBLE);
                                    frame4.setImageResource(R.drawable.piece4);
                                    count++;
                                } else {
                                    if ((dropped.getId() == R.id.piece5) && (dropTarget.getId() == R.id.frame5)) {
                                        dropped.setVisibility(View.INVISIBLE);
                                        frame5.setImageResource(R.drawable.piece5);
                                        count++;
                                    }
                                    else {
                                        if ((dropped.getId() == R.id.piece6) && (dropTarget.getId() == R.id.frame6)) {
                                            dropped.setVisibility(View.INVISIBLE);
                                            frame6.setImageResource(R.drawable.piece6);
                                            count++;
                                        }
                                        else {
                                            if ((dropped.getId() == R.id.piece7) && (dropTarget.getId() == R.id.frame7)) {
                                                dropped.setVisibility(View.INVISIBLE);
                                                frame7.setImageResource(R.drawable.piece7);
                                                count++;
                                            }
                                            else {
                                                if ((dropped.getId() == R.id.piece8) && (dropTarget.getId() == R.id.frame8)) {
                                                    dropped.setVisibility(View.INVISIBLE);
                                                    frame8.setImageResource(R.drawable.piece8);
                                                    count++;
                                                }
                                                else {
                                                    if ((dropped.getId() == R.id.piece9) && (dropTarget.getId() == R.id.frame9)) {
                                                        dropped.setVisibility(View.INVISIBLE);
                                                        frame9.setImageResource(R.drawable.piece9);
                                                        count++;
                                                    }
                                                    else
                                                    {
                                                        feedback.setImageResource(R.drawable.cross);
                                                        feedback.setVisibility(View.VISIBLE);
                                                        feedback.postDelayed(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                feedback.setVisibility(View.INVISIBLE);
                                                            }
                                                        }, 1500);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (count==9){
                        stars.setVisibility(View.VISIBLE);
                    }

                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }
    public void onBackPressed() {
        Intent intent = new Intent(puzzleLevel3Activity.this , puzzleLevel2Activity.class);
        startActivity(intent);
        finish();
    }
}
