package com.example.aep_project.dyslexiaapplication;

/**
 * Created by AEP-Project on 12/5/2014.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.HashMap;

public class DataRepo {
    private DBHelper dbHelper;

    public DataRepo(Context context) {
        dbHelper = new DBHelper(context);
    }

    public int insert(Data data) {

        //Open connection to write data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Data.KEY_DATE , data.date);
        values.put(Data.KEY_NAME, data.name);
        values.put(Data.KEY_LETTER, data.letter);
        values.put(Data.KEY_FIND, data.find);
        values.put(Data.KEY_FIND_RIGHT , data.find_right);
        values.put(Data.KEY_LISTEN, data.listen);
        values.put(Data.KEY_LISTEN_RIGHT, data.listen_right);
        values.put(Data.KEY_GESTURE, data.gesture);
        values.put(Data.KEY_GESTURE_RIGHT, data.gesture_right);

        // Inserting Row
        long data_Id = db.insert(Data.TABLE, null, values);
        db.close(); // Closing database connection
        return (int) data_Id;
    }

    public void delete(int data_Id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // It's a good practice to use parameter ?, instead of concatenate string
        db.delete(Data.TABLE, Data.KEY_ID + "= ?", new String[] { String.valueOf(data_Id) });
        db.close(); // Closing database connection
    }

    public void update(Data data) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Data.KEY_DATE , data.date);
        values.put(Data.KEY_NAME, data.name);
        values.put(Data.KEY_LETTER, data.letter);
        values.put(Data.KEY_FIND, data.find);
        values.put(Data.KEY_FIND_RIGHT, data.find_right);
        values.put(Data.KEY_LISTEN, data.listen);
        values.put(Data.KEY_LISTEN_RIGHT, data.listen_right);
        values.put(Data.KEY_GESTURE, data.gesture);
        values.put(Data.KEY_GESTURE_RIGHT, data.gesture_right);

        // It's a good practice to use parameter ?, instead of concatenate string
        db.update(Data.TABLE, values, Data.KEY_ID + "= ?", new String[] { String.valueOf(data.data_ID) });
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>>  getDataList() {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Data.KEY_ID      + "," +
                Data.KEY_DATE    + "," +
                Data.KEY_NAME    + "," +
                Data.KEY_LETTER  + "," +
                Data.KEY_FIND    + "," +
                Data.KEY_FIND_RIGHT + "," +
                Data.KEY_LISTEN     + "," +
                Data.KEY_LISTEN_RIGHT + "," +
                Data.KEY_GESTURE + "," +
                Data.KEY_GESTURE_RIGHT +
                " FROM " + Data.TABLE;

        ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> data = new HashMap<String, String>();
                data.put("date",cursor.getString(cursor.getColumnIndex(Data.KEY_DATE)));
                data.put("name", cursor.getString(cursor.getColumnIndex(Data.KEY_NAME)));
                data.put("letter",cursor.getString(cursor.getColumnIndex(Data.KEY_LETTER)));
                data.put("find", cursor.getString(cursor.getColumnIndex(Data.KEY_FIND)));
                data.put("find_right", cursor.getString(cursor.getColumnIndex(Data.KEY_FIND_RIGHT)));
                data.put("listen", cursor.getString(cursor.getColumnIndex(Data.KEY_LISTEN)));
                data.put("listen_right", cursor.getString(cursor.getColumnIndex(Data.KEY_LISTEN_RIGHT)));
                data.put("gesture",cursor.getString(cursor.getColumnIndex(Data.KEY_GESTURE)));
                data.put("gesture_right", cursor.getString(cursor.getColumnIndex(Data.KEY_GESTURE_RIGHT)));
                dataList.add(data);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return dataList;
    }

    public Data getDataById(int Id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery =  "SELECT  " +
                Data.KEY_ID      + "," +
                Data.KEY_DATE    + "," +
                Data.KEY_NAME    + "," +
                Data.KEY_LETTER  + "," +
                Data.KEY_FIND    + "," +
                Data.KEY_FIND_RIGHT + "," +
                Data.KEY_LISTEN  + "," +
                Data.KEY_LISTEN_RIGHT + "," +
                Data.KEY_GESTURE + "," +
                Data.KEY_GESTURE_RIGHT +
                " FROM " + Data.TABLE
                + " WHERE " +
                Data.KEY_ID + "=?";
        int iCount =0;
        Data data = new Data();

        Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(Id) } );

        if (cursor.moveToFirst()) {
            do {
                data.data_ID =cursor.getInt(cursor.getColumnIndex(Data.KEY_ID));
                data.date = cursor.getString(cursor.getColumnIndex(Data.KEY_DATE));
                data.name =cursor.getString(cursor.getColumnIndex(Data.KEY_NAME));
                data.letter  = cursor.getString(cursor.getColumnIndex(Data.KEY_LETTER));
                data.find = cursor.getInt(cursor.getColumnIndex(Data.KEY_FIND));
                data.find_right = cursor.getInt(cursor.getColumnIndex(Data.KEY_FIND_RIGHT));
                data.listen = cursor.getInt(cursor.getColumnIndex(Data.KEY_LISTEN));
                data.listen_right = cursor.getInt(cursor.getColumnIndex(Data.KEY_LISTEN_RIGHT));
                data.gesture = cursor.getInt(cursor.getColumnIndex(Data.KEY_GESTURE));
                data.gesture_right = cursor.getInt(cursor.getColumnIndex(Data.KEY_GESTURE_RIGHT));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return data;
    }
}