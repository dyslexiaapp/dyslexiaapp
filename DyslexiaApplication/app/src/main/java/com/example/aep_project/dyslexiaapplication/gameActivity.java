package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class gameActivity extends Activity implements View.OnClickListener{
    ImageView game1;
    ImageView game2;
    ImageView game3;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        game1=(ImageView) findViewById(R.id.game1);
        game2=(ImageView) findViewById(R.id.game2);
        game3=(ImageView) findViewById(R.id.game3);

        game1.setOnClickListener(this);
        game2.setOnClickListener(this);
        game3.setOnClickListener(this);

        back= (ImageView)findViewById(R.id.arrowBack);
        back.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.game1:
                Intent intent1 = new Intent(gameActivity.this , puzzleLevel1Activity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.game2:
                Intent intent2 = new Intent(gameActivity.this , mathActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.game3:
                Intent intent3 = new Intent (gameActivity.this,cardsActivity.class );
                startActivity(intent3);
                finish();
                break;
            case R.id.arrowBack:
                Intent intent = new Intent (gameActivity.this, chooseLetterActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
    public void onBackPressed() {
        Intent intent = new Intent(gameActivity.this, chooseLetterActivity.class);
        startActivity(intent);
        finish();
    }
}
