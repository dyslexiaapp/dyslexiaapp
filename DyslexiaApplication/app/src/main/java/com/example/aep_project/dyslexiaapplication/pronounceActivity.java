package com.example.aep_project.dyslexiaapplication;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.util.Log;
import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.widget.TextView;

import java.io.IOException;

public class pronounceActivity extends Activity implements View.OnClickListener{
    private static final String LOG_TAG = "AudioRecordTest";
    private static String mFileName = null;
    ImageButton mRecordButton;
    private MediaRecorder mRecorder = null;
    boolean mStartRecording = true;

    ImageButton    mPlayButton;
    private MediaPlayer   mPlayer = null;
    boolean mStartPlaying = true;

    int width;
    ImageView birdT1;
    ImageView birdT2;
    ImageView birdL1;
    ImageView birdL2;
    ImageView home;
    TextView info;

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    public pronounceActivity() {}

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_pronounce);

        GlobalData gd = (GlobalData)getApplication();
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/" + gd.getUserName()+"_"+gd.getLetter()+"_" + ".3gp";

        birdT1 = (ImageView) findViewById(R.id.tweeting1);
        birdT1.setVisibility(View.INVISIBLE);
        birdT2 = (ImageView) findViewById(R.id.tweeting2);
        birdT2.setVisibility(View.INVISIBLE);
        birdL1 = (ImageView) findViewById(R.id.listening1);
        birdL1.setVisibility(View.INVISIBLE);
        birdL2 = (ImageView) findViewById(R.id.listening2);
        birdL2.setVisibility(View.INVISIBLE);

        width = this.getResources().getDisplayMetrics().widthPixels;

        mRecordButton = (ImageButton) findViewById(R.id.startRecord);
        mRecordButton.setOnClickListener(this);

        mPlayButton = (ImageButton) findViewById(R.id.startPlay);
        mPlayButton.setOnClickListener(this);

        home = (ImageView) findViewById(R.id.home);
        home.setOnClickListener(this);

        info = (TextView)findViewById(R.id.info);
        info.setText(info.getText()+" "+gd.getLetter());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void translatePositionRight(ImageView image){
        TranslateAnimation animation = new TranslateAnimation(0.0f, width -140,0.0f, 0.0f);
        animation.setDuration(5000);
        animation.setRepeatCount(500);
        animation.setRepeatMode(2);
        animation.setFillAfter(true);
        image.startAnimation(animation);
    }

    public void translatePositionLeft(ImageView image){
        TranslateAnimation animation = new TranslateAnimation(0.0f, -width+100,0.0f, 0.0f);
        animation.setDuration(5000);
        animation.setRepeatCount(500);
        animation.setRepeatMode(2);
        animation.setFillAfter(true);
        image.startAnimation(animation);
    }

    public void flipp(ImageView image){
        ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(pronounceActivity.this, R.animator.flipping);
        anim.setTarget(image);
        anim.setDuration(10000);
        anim.setRepeatCount(500);
        anim.start();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.startPlay:
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                    mPlayButton.setImageResource(R.drawable.stop2);
                    birdT1.clearAnimation();
                    birdT2.clearAnimation();
                    birdL1.setVisibility(View.VISIBLE);
                    birdL2.setVisibility(View.VISIBLE);
                    birdT1.setVisibility(View.INVISIBLE);
                    birdT2.setVisibility(View.INVISIBLE);
                    translatePositionRight(birdL1);
                    flipp(birdL1);
                    translatePositionLeft(birdL2);
                    flipp(birdL2);
                    mRecordButton.setEnabled(false);
                } else {
                    mPlayButton.setImageResource(R.drawable.start2);
                    birdL1.clearAnimation();
                    birdL2.clearAnimation();
                    birdL1.setVisibility(View.INVISIBLE);
                    birdL2.setVisibility(View.INVISIBLE);
                    birdT1.setVisibility(View.INVISIBLE);
                    birdT2.setVisibility(View.INVISIBLE);
                    mRecordButton.setEnabled(true);
                }
                mStartPlaying = !mStartPlaying;
                break;
            case R.id.startRecord:
                onRecord(mStartRecording);
                if (mStartRecording) {
                    mRecordButton.setImageResource(R.drawable.stop1);
                    birdL1.clearAnimation();
                    birdL2.clearAnimation();
                    birdT1.setVisibility(View.VISIBLE);
                    birdT2.setVisibility(View.VISIBLE);
                    birdL1.setVisibility(View.INVISIBLE);
                    birdL2.setVisibility(View.INVISIBLE);
                    translatePositionRight(birdT1);
                    flipp(birdT1);
                    translatePositionLeft(birdT2);
                    flipp(birdT2);
                    mPlayButton.setEnabled(false);
                } else {
                    mRecordButton.setImageResource(R.drawable.start1);
                    birdT1.clearAnimation();
                    birdT2.clearAnimation();
                    birdL1.setVisibility(View.INVISIBLE);
                    birdL2.setVisibility(View.INVISIBLE);
                    birdT1.setVisibility(View.INVISIBLE);
                    birdT2.setVisibility(View.INVISIBLE);
                    mPlayButton.setEnabled(true);
                }
                mStartRecording = !mStartRecording;
                break;
            case R.id.home:
                Intent intent = new Intent (pronounceActivity.this , chooseActivity.class);
                startActivity(intent);
                finish();
                break;
        }

    }

    public void onBackPressed() {
        Intent intent = new Intent (pronounceActivity.this , chooseActivity.class);
        startActivity(intent);
        finish();
     }

}