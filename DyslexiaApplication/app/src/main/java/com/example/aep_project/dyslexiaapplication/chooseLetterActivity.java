package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class chooseLetterActivity extends Activity implements View.OnClickListener {

    ImageView a ;
    ImageView b;
    ImageView c;
    ImageView d;
    ImageView e;
    ImageView f;
    ImageView g;
    ImageView h;
    ImageView i;
    ImageView j;
    ImageView k;
    ImageView l;
    ImageView m;
    ImageView n;
    ImageView o;
    ImageView p;
    ImageView q;
    ImageView r;
    ImageView s;
    ImageView t;
    ImageView u;
    ImageView v;
    ImageView w;
    ImageView x;
    ImageView y;
    ImageView z;
    int videoID;
    int audioID;
    int listenID;
    String gestureID;
    int layoutID;
    GlobalData gd;
    ImageView game ;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_letter);
        gd = (GlobalData)getApplication();

        a = (ImageView)findViewById(R.id.choose_a);
        b = (ImageView)findViewById(R.id.choose_b);
        c = (ImageView)findViewById(R.id.choose_c);
        d = (ImageView)findViewById(R.id.choose_d);
        e = (ImageView)findViewById(R.id.choose_e);
        f = (ImageView)findViewById(R.id.choose_f);
        g = (ImageView)findViewById(R.id.choose_g);
        h = (ImageView)findViewById(R.id.choose_h);
        i = (ImageView)findViewById(R.id.choose_i);
        j = (ImageView)findViewById(R.id.choose_j);
        k = (ImageView)findViewById(R.id.choose_k);
        l = (ImageView)findViewById(R.id.choose_l);
        m = (ImageView)findViewById(R.id.choose_m);
        n = (ImageView)findViewById(R.id.choose_n);
        o = (ImageView)findViewById(R.id.choose_o);
        p = (ImageView)findViewById(R.id.choose_p);
        q = (ImageView)findViewById(R.id.choose_q);
        r = (ImageView)findViewById(R.id.choose_r);
        s = (ImageView)findViewById(R.id.choose_s);
        t = (ImageView)findViewById(R.id.choose_t);
        u = (ImageView)findViewById(R.id.choose_u);
        v = (ImageView)findViewById(R.id.choose_v);
        w = (ImageView)findViewById(R.id.choose_w);
        x = (ImageView)findViewById(R.id.choose_x);
        y = (ImageView)findViewById(R.id.choose_y);
        z = (ImageView)findViewById(R.id.choose_z);


        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);
        e.setOnClickListener(this);
        f.setOnClickListener(this);
        g.setOnClickListener(this);
        h.setOnClickListener(this);
        i.setOnClickListener(this);
        j.setOnClickListener(this);
        k.setOnClickListener(this);
        l.setOnClickListener(this);
        m.setOnClickListener(this);
        n.setOnClickListener(this);
        o.setOnClickListener(this);
        p.setOnClickListener(this);
        q.setOnClickListener(this);
        r.setOnClickListener(this);
        s.setOnClickListener(this);
        t.setOnClickListener(this);
        u.setOnClickListener(this);
        v.setOnClickListener(this);
        w.setOnClickListener(this);
        x.setOnClickListener(this);
        y.setOnClickListener(this);
        z.setOnClickListener(this);

        game = (ImageView) findViewById(R.id.game);
        game.setOnClickListener(this);

        back = (ImageView)findViewById(R.id.arrowBack);
        back.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose_letter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.choose_a:
                videoID = R.raw.lettera;
                gestureID = "letter_a";
                layoutID = R.layout.activity_find;
                audioID = R.raw.a_song;
                listenID = R.layout.activity_listen;
                gd.setLetter("letter A");
                break;
            case R.id.choose_b:
                videoID = R.raw.letterb;
                gestureID = "letter_b";
                layoutID = R.layout.find_b;
                audioID = R.raw.b_song;
                listenID = R.layout.listen_b;
                gd.setLetter("letter B");
                break;
            case R.id.choose_c:
                videoID = R.raw.letterc;
                gestureID = "letter_c";
                layoutID = R.layout.find_c;
                audioID = R.raw.c_song;
                listenID = R.layout.listen_c;
                gd.setLetter("letter C");
                break;
            case R.id.choose_d:
                videoID = R.raw.letterd;
                gestureID = "letter_d";
                layoutID = R.layout.find_d;
                audioID = R.raw.d_song;
                listenID = R.layout.listen_d;
                gd.setLetter("letter d");
                break;
            case R.id.choose_e:
                videoID = R.raw.lettere;
                gestureID = "letter_e";
                layoutID = R.layout.find_e;
                audioID = R.raw.e_song;
                listenID = R.layout.listen_e;
                gd.setLetter("letter E");
                break;
            case R.id.choose_f:
                videoID = R.raw.letterf;
                gestureID = "letter_f";
                layoutID = R.layout.find_f;
                audioID = R.raw.f_song;
                listenID = R.layout.listen_f;
                gd.setLetter("letter F");
                break;
            case R.id.choose_g:
                videoID = R.raw.letterg;
                gestureID = "letter_g";
                layoutID = R.layout.find_g;
                audioID = R.raw.g_song;
                listenID = R.layout.listen_g;
                gd.setLetter("letter G");
                break;
            case R.id.choose_h:
                videoID = R.raw.letterh;
                gestureID = "letter_h";
                layoutID = R.layout.find_h;
                audioID = R.raw.h_song;
                listenID = R.layout.listen_h;
                gd.setLetter("letter H");
                break;
            case R.id.choose_i:
                videoID = R.raw.letteri;
                gestureID = "letter_i";
                layoutID = R.layout.find_i;
                audioID = R.raw.i_song;
                listenID = R.layout.listen_i;
                gd.setLetter("letter I");
                break;
            case R.id.choose_j:
                videoID = R.raw.letterj;
                gestureID = "letter_j";
                layoutID = R.layout.find_j;
                audioID = R.raw.j_song;
                listenID = R.layout.listen_j;
                gd.setLetter("letter J");
                break;
            case R.id.choose_k:
                videoID = R.raw.letterk;
                gestureID = "letter_k";
                layoutID = R.layout.find_k;
                audioID = R.raw.k_song;
                listenID = R.layout.listen_k;
                gd.setLetter("letter K");
                break;
            case R.id.choose_l:
                videoID = R.raw.letterl;
                gestureID = "letter_l";
                layoutID = R.layout.find_l;
                audioID = R.raw.l_song;
                listenID = R.layout.listen_l;
                gd.setLetter("letter L");
                break;
            case R.id.choose_m:
                videoID = R.raw.letterm;
                gestureID = "letter_m";
                layoutID = R.layout.find_m;
                audioID = R.raw.m_song;
                listenID = R.layout.listen_m;
                gd.setLetter("letter M");
                break;
            case R.id.choose_n:
                videoID = R.raw.lettern;
                gestureID = "letter_n";
                layoutID = R.layout.find_n;
                audioID = R.raw.n_song;
                listenID = R.layout.listen_n;
                gd.setLetter("letter N");
                break;
            case R.id.choose_o:
                videoID = R.raw.lettero;
                gestureID = "letter_o";
                layoutID = R.layout.find_o;
                audioID = R.raw.o_song;
                listenID = R.layout.listen_o;
                gd.setLetter("letter O");
                break;
            case R.id.choose_p:
                videoID = R.raw.letterp;
                gestureID = "letter_p";
                layoutID = R.layout.find_p;
                audioID = R.raw.p_song;
                listenID = R.layout.listen_p;
                gd.setLetter("letter P");
                break;
            case R.id.choose_q:
                videoID = R.raw.letterq;
                gestureID = "letter_q";
                layoutID = R.layout.find_q;
                audioID = R.raw.q_song;
                listenID = R.layout.listen_q;
                gd.setLetter("letter Q");
                break;
            case R.id.choose_r:
                videoID = R.raw.letterr;
                gestureID = "letter_r";
                layoutID = R.layout.find_r;
                audioID = R.raw.r_song;
                listenID = R.layout.listen_r;
                gd.setLetter("letter R");
                break;
            case R.id.choose_s:
                videoID = R.raw.letters;
                gestureID = "letter_s";
                layoutID = R.layout.find_s;
                audioID = R.raw.s_song;
                listenID = R.layout.listen_s;
                gd.setLetter("letter S");
                break;
            case R.id.choose_t:
                videoID = R.raw.lettert;
                gestureID = "letter_t";
                layoutID = R.layout.find_t;
                audioID = R.raw.t_song;
                listenID = R.layout.listen_t;
                gd.setLetter("letter T");
                break;
            case R.id.choose_u:
                videoID = R.raw.letteru;
                gestureID = "letter_u";
                layoutID = R.layout.find_u;
                audioID = R.raw.u_song;
                listenID = R.layout.listen_u;
                gd.setLetter("letter U");
                break;
            case R.id.choose_v:
                videoID = R.raw.letterv;
                gestureID = "letter_v";
                layoutID = R.layout.find_v;
                audioID = R.raw.v_song;
                listenID = R.layout.listen_v;
                gd.setLetter("letter V");
                break;
            case R.id.choose_w:
                videoID = R.raw.letterw;
                gestureID = "letter_w";
                layoutID = R.layout.find_w;
                audioID = R.raw.w_song;
                listenID = R.layout.listen_w;
                gd.setLetter("letter W");
                break;
            case R.id.choose_x:
                videoID = R.raw.letterx;
                gestureID = "letter_x";
                layoutID = R.layout.find_x;
                audioID = R.raw.x_song;
                listenID = R.layout.listen_x;
                gd.setLetter("letter X");
                break;
            case R.id.choose_y:
                videoID = R.raw.lettery;
                gestureID = "letter_y";
                layoutID = R.layout.find_y;
                audioID = R.raw.y_song;
                listenID = R.layout.listen_y;
                gd.setLetter("letter Y");
                break;
            case R.id.choose_z:
                videoID = R.raw.letterz;
                gestureID = "letter_z";
                layoutID = R.layout.find_z;
                audioID = R.raw.z_song;
                listenID = R.layout.listen_z;
                gd.setLetter("letter Z");
                break;
            case R.id.game:
                Intent intentG = new Intent(chooseLetterActivity.this, gameActivity.class);
                startActivity(intentG);
                finish();
                break;
            case R.id.arrowBack:
                Intent intentM = new Intent (chooseLetterActivity.this , mainActivity.class);
                startActivity(intentM);
                finish();
                break;
        }
        if(v.getId()!=R.id.game && v.getId()!=R.id.arrowBack)
            startChooseActivity();
    }

    public void startChooseActivity(){
        Intent intent = new Intent(chooseLetterActivity.this, chooseActivity.class);
        intent.putExtra("videoID", videoID);
        intent.putExtra("gestureID", gestureID);
        intent.putExtra("layoutID" , layoutID);
        intent.putExtra("audioID" , audioID);
        intent.putExtra("listenID" , listenID);
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent (chooseLetterActivity.this, mainActivity.class);
        startActivity(intent);
        finish();
    }


}
