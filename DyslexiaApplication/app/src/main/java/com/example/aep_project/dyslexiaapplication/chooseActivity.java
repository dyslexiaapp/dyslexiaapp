package com.example.aep_project.dyslexiaapplication;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import java.util.Date;
import java.util.GregorianCalendar;

public class chooseActivity extends ActionBarActivity implements View.OnClickListener{
    int videoID;
    int layoutID;
    int audioID;
    int listenID;
    String gestureID;
    ImageView _write;
    ImageView _find;
    ImageView _listen;
    ImageView _pronounce;
    ImageView backArrow;

    int _data_Id = 0;
    String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Intent intent = getIntent();
        videoID = intent.getIntExtra("videoID", 0);
        audioID = intent.getIntExtra("audioID", 0);
        gestureID = intent.getStringExtra("gestureID");
        layoutID = intent.getIntExtra("layoutID", 0);
        listenID = intent.getIntExtra("listenID" , 0);

        _write = (ImageView)findViewById(R.id.write);
        _find = (ImageView) findViewById(R.id.find);
        _listen = (ImageView) findViewById(R.id.listen);
        _pronounce = (ImageView)findViewById(R.id.pronounce);
        _write.setOnClickListener(this);
        _find.setOnClickListener(this);
        _listen.setOnClickListener(this);
        _pronounce.setOnClickListener(this);
        backArrow = (ImageView)findViewById(R.id.arrowBack);
        backArrow.setOnClickListener(this);

        GregorianCalendar c = new GregorianCalendar();
        Date todays_date = c.getTime();
        date = todays_date.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.write:
                Intent intent1 = new Intent(chooseActivity.this, writeActivity.class);
                intent1.putExtra("videoID", videoID);
                intent1.putExtra("gestureID" , gestureID);
                startActivity(intent1);
                break;
            case R.id.find:
                Intent intent2 = new  Intent(chooseActivity.this , findActivity.class);
                intent2.putExtra("layoutID" , layoutID);
                startActivity(intent2);
                break;
            case R.id.listen:
                Intent intent3 = new  Intent(chooseActivity.this , listenActivity.class);
                intent3.putExtra("audioID", audioID);
                intent3.putExtra("listenID" , listenID);
                startActivity(intent3);
                break;
            case R.id.pronounce:
                Intent intent4 = new Intent(chooseActivity.this, pronounceActivity.class);
                startActivity(intent4);
                break;
            case R.id.arrowBack:
                saveToDatabase();
                Intent intent = new Intent (chooseActivity.this, chooseLetterActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onBackPressed() {
        saveToDatabase();
        Intent intent = new Intent (chooseActivity.this, chooseLetterActivity.class);
        startActivity(intent);
        finish();
    }

    public void saveToDatabase (){
        GlobalData gd = (GlobalData)getApplication();
        DataRepo repo = new DataRepo(this);
        Data data = new Data();
        data.data_ID=_data_Id;
        data.date = date;
        data.name = gd.getUserName();
        data.letter = gd.getLetter();
        data.find = gd.getFindScore();
        data.find_right = gd.getFindRight();
        data.listen = gd.getListenScore();
        data.listen_right=gd.getListenRight();
        data.gesture = gd.getGestureTrials();
        data.gesture_right = gd.getGestureRight();

        if(_data_Id==0)
            _data_Id = repo.insert(data);
        else
            repo.update(data);

        gd.clearMemory();
    }
}


