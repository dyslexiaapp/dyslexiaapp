package com.example.aep_project.dyslexiaapplication;

import android.app.Application;

/**
 * Created by AEP-Project on 12/5/2014.
 */
public class GlobalData extends Application{
    private int findScore;
    private int findRight;
    private int listenScore;
    private int listenRight;
    private int gestureTrials;
    private int gestureRight;
    private String letter;
    private String userName;

    public int getFindScore() { return findScore;}
    public void setFindScore(int score) { this.findScore = score;}

    public int getFindRight() { return findRight;}
    public void setFindRight(int score) { this.findRight = score;}

    public int getListenScore() { return listenScore;}
    public void setListenScore(int score) {this.listenScore = score;}

    public int getListenRight() { return listenRight;}
    public void setListenRight(int score) {this.listenRight = score;}

    public int getGestureTrials() {return gestureTrials;}
    public void setGestureTrials(int trials) {this.gestureTrials = trials;}

    public int getGestureRight() {return gestureRight;}
    public void setGestureRight(int rightTrials) {this.gestureRight = rightTrials;}

    public String getLetter(){return letter;}
    public void setLetter(String letter){this.letter = letter;}

    public String getUserName(){return userName;}
    public void setUserName(String user){this.userName = user;}

    public void clearMemory(){
        this.findScore =0;
        this.findRight=0;
        this.listenScore=0;
        this.listenRight=0;
        this.gestureTrials=0;
        this.gestureRight=0;

    }
}