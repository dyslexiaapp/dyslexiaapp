package com.example.aep_project.dyslexiaapplication;

import java.sql.Blob;

/**
 * Created by AEP-Project on 12/5/2014.
 */
public class Data {
    // Labels table name
    public static final String TABLE = "Data";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_DATE = "date";
    public static final String KEY_NAME = "name";
    public static final String KEY_LETTER = "letter";
    public static final String KEY_FIND = "find";
    public static final String KEY_FIND_RIGHT = "find_right";
    public static final String KEY_LISTEN = "listen";
    public static final String KEY_LISTEN_RIGHT = "listen_right";
    public static final String KEY_GESTURE = "gesture";
    public static final String KEY_GESTURE_RIGHT = "gesture_right";

    // property help us to keep data
    public int data_ID;
    public String date;
    public String name;
    public String letter;
    public int find;
    public int find_right;
    public int listen;
    public int listen_right;
    public int gesture;
    public int gesture_right;
}