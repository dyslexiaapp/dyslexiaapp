package com.example.aep_project.dyslexiaapplication;

import android.content.ClipData;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class listenActivity extends ActionBarActivity implements View.OnClickListener {
    int count_trials = 0;
    int count_right = 0;
    private ImageView ballon1, ballon2, ballon3 , ballon4, right1 , right2;
    ImageView box;
    ImageView feedback;
    int audioID;
    int listenID;
    MediaPlayer mp = null;
    ImageView click;
    int clickNr = 0;
    ImageView home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        audioID = intent.getIntExtra("audioID" , 0);
        listenID = intent.getIntExtra("listenID" , 0);
        setContentView(listenID);

        //views to drag
        ballon1 = (ImageView)findViewById(R.id.ballon1);
        ballon2 = (ImageView)findViewById(R.id.ballon2);
        ballon3 = (ImageView)findViewById(R.id.ballon3);
        ballon4 = (ImageView)findViewById(R.id.ballon4);
        right1 = (ImageView)findViewById(R.id.right1);
        right2 = (ImageView)findViewById(R.id.right2);
        box = (ImageView)findViewById(R.id.box);
        feedback = (ImageView)findViewById(R.id.feedback);
        click =(ImageView)findViewById(R.id.hand);

        click.setVisibility(View.VISIBLE);
        click.postDelayed(new Runnable() {
            @Override
            public void run() {
                click.setVisibility(View.INVISIBLE);
            }
        }, 5000);

        ChoiceTouchListener choice = new ChoiceTouchListener();
        //set touch listeners
        ballon1.setOnTouchListener(choice);
        ballon2.setOnTouchListener(choice);
        ballon3.setOnTouchListener(choice);
        ballon4.setOnTouchListener(choice);
        right1.setOnTouchListener(choice);
        right2.setOnTouchListener(choice);

        //set drag listeners
        ChoiceDragListener drag = new ChoiceDragListener();

        box.setOnDragListener(drag);

        box.setOnClickListener(this);
        home = (ImageView) findViewById(R.id.home);
        home.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.listen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.box:
                clickNr++;
                managerOfSound(audioID, clickNr);
                break;
            case R.id.home:
                if(mp!=null){
                    mp.stop();
                    mp.release();
                    mp=null;
                }
                GlobalData gd = (GlobalData) getApplication();
                gd.setListenScore(count_trials+gd.getListenScore());
                gd.setListenRight(count_right+gd.getListenRight());
                Intent intent = new Intent (listenActivity.this , chooseActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private class ChoiceTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                //setup drag

                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            }
            else {
                return false;
            }
        }
    }

    private class ChoiceDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {

            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:

                    View view = (View) dragEvent.getLocalState();
                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;
                    //view being dragged and dropped
                    ImageView dropped = (ImageView) view;

                    if ((( dropped.getId()== R.id.right1)|| (dropped.getId()==R.id.right2))&&(dropTarget.getId()==R.id.box)) {

                        view.setVisibility(View.INVISIBLE);
                        feedback.setImageResource(R.drawable.stars3);
                        feedback.setVisibility(View.VISIBLE);
                        feedback.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                feedback.setVisibility(View.INVISIBLE);
                            }
                        }, 1500);
                        if(count_right!=2){
                           count_trials++;
                           count_right++;}
                        }
                    else {
                        feedback.setImageResource(R.drawable.cross);
                        feedback.setVisibility(View.VISIBLE);
                        feedback.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                feedback.setVisibility(View.INVISIBLE);
                            }
                        }, 1500);
                        if(count_right!=2)
                           count_trials++;
                    }
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary

                    break;
                default:
                    break;
            }
            return true;
        }
    }

    protected void managerOfSound(int Id, int click) {
        if (mp != null) {
            mp.reset();
            mp.release();
        }
        else {
            mp = MediaPlayer.create(this, Id);
        }
        if (click%2==0){
            mp.release();
            mp = null;}
        else
            mp.start();

    }

    public void onBackPressed() {
        if(mp!=null){
            mp.stop();
            mp.release();
            mp=null;
        }
        Intent intent = new Intent (listenActivity.this , chooseActivity.class);
        startActivity(intent);
        GlobalData gd = (GlobalData)getApplication();
        gd.setListenScore(count_trials+gd.getListenScore());
        gd.setListenRight(count_right+gd.getListenRight());
        finish();
    }
}

