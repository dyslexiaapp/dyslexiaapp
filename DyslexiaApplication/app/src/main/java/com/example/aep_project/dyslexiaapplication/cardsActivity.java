package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class cardsActivity extends Activity implements View.OnClickListener {
    ImageView image1;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    ImageView image5;
    ImageView image6;
    ImageView menu;
    ImageView stars;
    ImageView greeting;
    int count_2_click = 0;
    int idClicked[] = new int[2];
    int  width;
    int height;
    boolean match1 = false;
    boolean match2 = false;
    boolean match3 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);

        image1 = (ImageView) findViewById(R.id.position1);
        image2 = (ImageView) findViewById(R.id.position2);
        image3 = (ImageView) findViewById(R.id.position3);
        image4 = (ImageView) findViewById(R.id.position4);
        image5 = (ImageView) findViewById(R.id.position5);
        image6 = (ImageView) findViewById(R.id.position6);
        stars = (ImageView) findViewById(R.id.stars);
        menu = (ImageView)findViewById(R.id.menu);
        greeting = (ImageView) findViewById(R.id.greeting);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
        image5.setOnClickListener(this);
        image6.setOnClickListener(this);
        menu.setOnClickListener(this);

        width = this.getResources().getDisplayMetrics().widthPixels;
        height = this.getResources().getDisplayMetrics().heightPixels;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cards, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                Intent intent = new Intent (cardsActivity.this, gameActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.position1:
                image1.setImageResource(R.drawable.cardm);
                actClickInRow(R.id.position1);

                break;
            case R.id.position2:
                image2.setImageResource(R.drawable.rabbit);
                actClickInRow(R.id.position2);

                break;
            case R.id.position3:
                image3.setImageResource(R.drawable.cardp);
                actClickInRow(R.id.position3);

                break;
            case R.id.position4:
                image4.setImageResource(R.drawable.pigeon);
                actClickInRow(R.id.position4);

                break;
            case R.id.position5:
                image5.setImageResource(R.drawable.magician);
                actClickInRow(R.id.position5);

                break;
            case R.id.position6:
                image6.setImageResource(R.drawable.cardr);
                actClickInRow(R.id.position6);
                break;
        }
    }

    public void actClickInRow(int imageID) {
        if (count_2_click < 2) {
            idClicked[count_2_click] = imageID;
            count_2_click++;
        }
        if (count_2_click == 2) {
            disableClick();
            if ((idClicked[0] == R.id.position1 || idClicked[1] == R.id.position1) && (idClicked[1] == R.id.position5 || idClicked[0] == R.id.position5)) {
                stars.setVisibility(View.VISIBLE);
                match1=true;
                image1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image1.setVisibility(View.INVISIBLE);
                        stars.setVisibility(View.INVISIBLE);
                        enableClick();
                    }
                },1500);

                image5.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image5.setVisibility(View.INVISIBLE);
                    }
                }, 1500);

            } else {
                image1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image1.setImageResource(R.drawable.card);
                        enableClick();
                    }
                },1500);
                image5.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image5.setImageResource(R.drawable.card);
                    }
                },1500);

            }
            if ((idClicked[0] == R.id.position2 || idClicked[1] == R.id.position2) && (idClicked[1] == R.id.position6 || idClicked[0] == R.id.position6)) {
                stars.setVisibility(View.VISIBLE);
                match2 = true;
                image2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image2.setVisibility(View.INVISIBLE);
                        stars.setVisibility(View.INVISIBLE);
                        enableClick();
                    }
                },1500);
                image6.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image6.setVisibility(View.INVISIBLE);
                    }
                },1500);
            } else {

                image2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image2.setImageResource(R.drawable.card);
                        enableClick();
                    }
                },1500);
                image6.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image6.setImageResource(R.drawable.card);
                    }
                },1500);
            }
            if ((idClicked[0] == R.id.position3 || idClicked[1] == R.id.position3) && (idClicked[1] == R.id.position4 || idClicked[0] == R.id.position4)) {
                match3=true;
                stars.setVisibility(View.VISIBLE);
                image3.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image3.setVisibility(View.INVISIBLE);
                        stars.setVisibility(View.INVISIBLE);
                        enableClick();
                    }
                },1500);
                image4.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image4.setVisibility(View.INVISIBLE);

                    }
                },1500);

            } else {

                image3.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image3.setImageResource(R.drawable.card);
                        enableClick();
                    }
                },1500);
                image4.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image4.setImageResource(R.drawable.card);
                    }
                },1500);

            }
            count_2_click = 0;
        }

        if (count_2_click > 2) {
            count_2_click = 0;
        }
        if(match1 && match2 && match3) {
            greeting.postDelayed(new Runnable() {
                @Override
                public void run() {
                    greeting.setVisibility(View.VISIBLE);
                }
            }, 2000) ;
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(cardsActivity.this , gameActivity.class);
        startActivity(intent);
        finish();
    }

    public void disableClick(){
        image1.setEnabled(false);
        image2.setEnabled(false);
        image3.setEnabled(false);
        image4.setEnabled(false);
        image5.setEnabled(false);
        image6.setEnabled(false);
    }

    public void enableClick(){
        image1.setEnabled(true);
        image2.setEnabled(true);
        image3.setEnabled(true);
        image4.setEnabled(true);
        image5.setEnabled(true);
        image6.setEnabled(true);
    }
}
