package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class writeActivity extends Activity implements MediaPlayer.OnCompletionListener , View.OnTouchListener, View.OnClickListener {
    String code = "writeActivity";
    int videoID;
    String gestureID;
    MediaPlayer mp = null;
    VideoView videoHolder;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);
        Intent intent = getIntent();
        gestureID = intent.getStringExtra("gestureID");
        videoID = intent.getIntExtra("videoID" , 0);
        try {
            splashPlayer();
        } catch (Exception ex) {
            jump();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    public void splashPlayer() {
        videoHolder = new VideoView(this);
        setContentView(videoHolder);
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/"+ videoID);
        videoHolder.setVideoURI(video);
        videoHolder.setOnCompletionListener(this);
        MediaController vidControl = new MediaController(this){
            @Override
            public void hide() {
                this.show(0);
            }

            @Override
            public void setMediaPlayer(MediaPlayerControl player) {
                super.setMediaPlayer(player);
                this.show();
            }
        };

        vidControl.setAnchorView(videoHolder);
        videoHolder.setMediaController(vidControl);
        videoHolder.start();
        videoHolder.setOnTouchListener(this);
    }

    private synchronized void jump() {
        if(mp!=null){
            mp.stop();
            mp.release();
            mp=null;
        }
        Intent intent = new Intent(writeActivity.this, gestureActivity.class);
        intent.putExtra("gestureID", gestureID);
        intent.putExtra("videoID", videoID);
        startActivity(intent);
        finish();
    }

    private synchronized void jumpHome() {
        if(mp!=null){
            mp.stop();
            mp.release();
            mp=null;
        }
        Intent intent = new Intent(writeActivity.this, chooseActivity.class);
        intent.putExtra("code" , code);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        jump();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ((VideoView) v).stopPlayback();
        jumpHome();
        return true;
    }

   public void onBackPressed() {
        super.onBackPressed();
        videoHolder.stopPlayback();
        jumpHome();
    }



    @Override
    public void onClick(View v) {}
}



