package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class findActivity extends Activity implements View.OnClickListener {
    TextView question;
    TextView answer;
    ImageButton button1;
    ImageButton button2;
    ImageButton button3;
    ImageButton right;
    ImageView image1;
    ImageView image2;
    ImageView home;
    ImageView feedback;
    int layoutID;
    int count_trials=0;
    int count_right=0;
    boolean stopCounting = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        layoutID = intent.getIntExtra("layoutID" , 0);
        setContentView(layoutID);
        button1 = (ImageButton)findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2 = (ImageButton)findViewById(R.id.button2);
        button2.setOnClickListener(this);
        button3 = (ImageButton)findViewById(R.id.button3);
        button3.setOnClickListener(this);
        right = (ImageButton)findViewById(R.id.right);
        right.setOnClickListener(this);
        question = (TextView)findViewById(R.id.question);
        answer = (TextView)findViewById(R.id.answer);
        image1 = (ImageView)findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        home = (ImageView) findViewById(R.id.home);
        home.setOnClickListener(this);
        feedback = (ImageView)findViewById(R.id.feedback);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.find, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void btnRightClick(){
        count_right++;
        if(!stopCounting)
            count_trials++;
        stopCounting = true;
        question.setVisibility(View.INVISIBLE);
        answer.setVisibility(View.VISIBLE);
        if(count_trials==1) {
            feedback.setImageResource(R.drawable.stars4);
            feedback.setVisibility(View.VISIBLE);
            feedback.postDelayed(new Runnable() {
                @Override
                public void run() {
                    feedback.setVisibility(View.INVISIBLE);
                }
            }, 1500);
        }
        else{
            if(count_trials==2){
                {
                    feedback.setImageResource(R.drawable.stars3);
                    feedback.setVisibility(View.VISIBLE);
                    feedback.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            feedback.setVisibility(View.INVISIBLE);
                        }
                    }, 1500);
                }
            }


            else
            {
                feedback.setImageResource(R.drawable.stars2);
                feedback.setVisibility(View.VISIBLE);
                feedback.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feedback.setVisibility(View.INVISIBLE);
                    }
                }, 1500);
            }
        }
        image1.setVisibility(View.VISIBLE);
        image2.setVisibility(View.INVISIBLE);

    }

    private void btnClick() {
        if(!stopCounting)
            count_trials++;

        image1.setVisibility(View.INVISIBLE);
        image2.setVisibility(View.VISIBLE);
        question.setVisibility(View.VISIBLE);
        answer.setVisibility(View.INVISIBLE);
        feedback.setImageResource(R.drawable.cross);
        feedback.setVisibility(View.VISIBLE);
        feedback.postDelayed(new Runnable() {
            @Override
            public void run() {
                feedback.setVisibility(View.INVISIBLE);
            }
        }, 1500);

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.button1:
                btnClick();
                break;
            case R.id.button2:
                btnClick();
                break;
            case R.id.button3:
                btnClick();
                break;
            case R.id.right:
                btnRightClick();
                break;
            case R.id.home:
                Intent intent = new Intent(findActivity.this , chooseActivity.class);
                startActivity(intent);
                GlobalData gd = (GlobalData)getApplication();
                gd.setFindScore(count_trials+gd.getFindScore());
                gd.setFindRight(count_right+gd.getFindRight());
                finish();
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(findActivity.this , chooseActivity.class);
        startActivity(intent);
        GlobalData gd = (GlobalData)getApplication();
        gd.setFindScore(count_trials + gd.getFindScore());
        gd.setFindRight(count_right + gd.getFindRight());
        finish();
    }
}
