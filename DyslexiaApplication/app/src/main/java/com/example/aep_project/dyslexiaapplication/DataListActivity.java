package com.example.aep_project.dyslexiaapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;

public class DataListActivity extends ListActivity implements View.OnClickListener {
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_list);

        DataRepo repo = new DataRepo(this);
        final ArrayList<HashMap<String, String>> dataList =  repo.getDataList();
        if(dataList.size()!=0) {
            ListAdapter adapter = new SimpleAdapter( DataListActivity.this, dataList, R.layout.data_view,
                    new String[] { "date"    ,"name"         , "letter"         ,"find"         ,"find_right"     , "listen"        ,"listen_right"     ,"gesture_right"         ,"gesture"},
                    new int[] {R.id.data_date, R.id.data_name,  R.id.data_letter, R.id.data_find, R.id.choicesFind, R.id.data_listen, R.id.choicesListen, R.id.data_gesture_right, R.id.data_gesture});
            setListAdapter(adapter);
        }else{
            Toast.makeText(this, "No data!", Toast.LENGTH_SHORT).show();
        }
        back = (ImageView)findViewById(R.id.arrowBack);
        back.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.data_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        Intent intent = new Intent(DataListActivity.this , mainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.arrowBack){
            Intent intent = new Intent(DataListActivity.this, mainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}


