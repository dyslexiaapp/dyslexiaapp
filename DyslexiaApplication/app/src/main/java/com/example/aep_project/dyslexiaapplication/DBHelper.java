package com.example.aep_project.dyslexiaapplication;

/**
 * Created by AEP-Project on 12/5/2014.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper  extends SQLiteOpenHelper {
    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "data.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_DATA = "CREATE TABLE " + Data.TABLE  + "("
                + Data.KEY_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Data.KEY_DATE + " TEXT, "
                + Data.KEY_NAME + " TEXT, "
                + Data.KEY_LETTER + " TEXT, "
                + Data.KEY_FIND + " INTEGER, "
                + Data.KEY_FIND_RIGHT + " INTEGER, "
                + Data.KEY_LISTEN + " INTEGER, "
                + Data.KEY_LISTEN_RIGHT + " INTEGER, "
                + Data.KEY_GESTURE + " INTEGER, "
                + Data.KEY_GESTURE_RIGHT + " INTEGER )";

        db.execSQL(CREATE_TABLE_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Data.TABLE);
        // Create tables again
        onCreate(db);
    }

}
