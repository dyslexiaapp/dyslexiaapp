package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher;


public class mathActivity extends Activity implements View.OnClickListener {


    ImageSwitcher imageSwitcher;
    ImageView button1,button2, button3, button4, button5, button6, button7, button8, button9, button10, stars;
    RelativeLayout l1;
    ImageView feedback;
    GridLayout g1;
    Animation slide_in_left, slide_out_right;

    int imageResources[] = {
            R.drawable.butterfly,
            R.drawable.bees,
            R.drawable.butterflies9,
            R.drawable.spiders,
            R.drawable.butterflies10,
            R.drawable.bee,
            R.drawable.butterflies5,
            R.drawable.ladybugs,
            R.drawable.dragonfly,
            R.drawable.ants
    };
    int curIndex;
    ImageView home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math);

        button1 = (ImageView)findViewById(R.id.button1);
        button2 = (ImageView)findViewById(R.id.button2);
        button3 = (ImageView)findViewById(R.id.button3);
        button4 = (ImageView)findViewById(R.id.button4);
        button5 = (ImageView)findViewById(R.id.button5);
        button6 = (ImageView)findViewById(R.id.button6);
        button7 = (ImageView)findViewById(R.id.button7);
        button8 = (ImageView)findViewById(R.id.button8);
        button9 = (ImageView)findViewById(R.id.button9);
        button10 =(ImageView)findViewById(R.id.button10);
        home = (ImageView)findViewById(R.id.home);
        stars = (ImageView)findViewById(R.id.stars);
        feedback = (ImageView)findViewById(R.id.feedback);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button10.setOnClickListener(this);
       home.setOnClickListener(this);


        imageSwitcher = (ImageSwitcher) findViewById(R.id.imageswitcher);
        l1 = (RelativeLayout)findViewById(R.id.r1);
        g1 = (GridLayout)findViewById(R.id.g1);

        slide_in_left = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_out_right);

        imageSwitcher.setInAnimation(slide_in_left);
        imageSwitcher.setOutAnimation(slide_out_right);

        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {

                ImageView imageView = new ImageView(mathActivity.this);


                ViewGroup.LayoutParams params = new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                imageView.setLayoutParams(params);
                return imageView;

            }
        });
        curIndex = 0;
        imageSwitcher.setImageResource(imageResources[curIndex]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.math, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.home){
            Intent intent = new Intent (mathActivity.this , gameActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            if ((v.getId() == R.id.button1) && (curIndex == 5)) {
                button1.setVisibility(View.INVISIBLE);
                imageSwitcher.setImageResource(imageResources[++curIndex]);
            } else {
                if ((v.getId() == R.id.button2) && (curIndex == 0)) {
                    button2.setVisibility(View.INVISIBLE);
                    imageSwitcher.setImageResource(imageResources[++curIndex]);
                } else {
                    if ((v.getId() == R.id.button3) && (curIndex == 8)) {
                        button3.setVisibility(View.INVISIBLE);
                        imageSwitcher.setImageResource(imageResources[++curIndex]);
                    } else {
                        if ((v.getId() == R.id.button4) && (curIndex == 9)) {
                            button4.setVisibility(View.INVISIBLE);
                            stars.setVisibility(View.VISIBLE);
                            imageSwitcher.setVisibility(View.INVISIBLE);
                        } else {
                            if ((v.getId() == R.id.button5) && (curIndex == 6)) {
                                button5.setVisibility(View.INVISIBLE);
                                imageSwitcher.setImageResource(imageResources[++curIndex]);
                            } else {
                                if ((v.getId() == R.id.button6) && (curIndex == 3)) {
                                    button6.setVisibility(View.INVISIBLE);
                                    imageSwitcher.setImageResource(imageResources[++curIndex]);
                                } else {
                                    if ((v.getId() == R.id.button7) && (curIndex == 7)) {
                                        button7.setVisibility(View.INVISIBLE);
                                        imageSwitcher.setImageResource(imageResources[++curIndex]);
                                    } else {
                                        if ((v.getId() == R.id.button8) && (curIndex == 1)) {
                                            button8.setVisibility(View.INVISIBLE);
                                            imageSwitcher.setImageResource(imageResources[++curIndex]);
                                        } else {
                                            if ((v.getId() == R.id.button9) && (curIndex == 2)) {
                                                button9.setVisibility(View.INVISIBLE);
                                                imageSwitcher.setImageResource(imageResources[++curIndex]);
                                            } else {
                                                if ((v.getId() == R.id.button10) && (curIndex == 4)) {
                                                    button10.setVisibility(View.INVISIBLE);
                                                    imageSwitcher.setImageResource(imageResources[++curIndex]);

                                                } else {
                                                    feedback.setImageResource(R.drawable.cross);
                                                    feedback.setVisibility(View.VISIBLE);
                                                    feedback.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            feedback.setVisibility(View.INVISIBLE);
                                                        }
                                                    }, 1500);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
    public void onBackPressed() {
        Intent intent = new Intent(mathActivity.this , gameActivity.class);
        startActivity(intent);
        finish();
    }
}
