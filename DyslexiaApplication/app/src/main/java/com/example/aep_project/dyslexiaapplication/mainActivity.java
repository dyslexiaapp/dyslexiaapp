package com.example.aep_project.dyslexiaapplication;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class mainActivity extends Activity implements View.OnClickListener,AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener{
    ImageView addImage;
    ImageView parent;
    ArrayList<User> imageArry = new ArrayList<User>();
    UserImageAdapter imageAdapter;
    private static final int PICK_FROM_GALLERY = 1;
    ListView dataList;
    byte[] imageName;
    int imageId;
    DataBaseHandler db;
    String name;

    //Called when the activity is first created.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dataList = (ListView) findViewById(R.id.list);

        //create DatabaseHandler object
        db = new DataBaseHandler(this);

        //Reading and getting all records from database
        List<User> users = db.getAllUsers();
        for (User cn : users) {
            String log = "ID:" + cn.getID() + " Name: " + cn.getName() + " ,Image: " + cn.getImage();

            // Writing Contacts to log
            Log.d("Result: ", log);
            // add contacts data in arrayList
            imageArry.add(cn);
        }
        //Set Data base Item into listview
        imageAdapter = new UserImageAdapter(this, R.layout.screen_list, imageArry);
        dataList.setAdapter(imageAdapter);
        dataList.setOnItemClickListener(this);
        dataList.setOnItemLongClickListener(this);

        addImage = (ImageView) findViewById(R.id.btnAdd);
        addImage.setOnClickListener(this);

        parent = (ImageView)findViewById(R.id.parent);
        parent.setOnClickListener(this);

    }

    //On activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        GlobalData g = (GlobalData) getApplication();
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                Bundle extras = data.getExtras();
                Bitmap yourImage = extras.getParcelable("data");
                // convert bitmap to byte
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                byte imageInByte2[] = stream2.toByteArray();
                Log.e("output before conversion", imageInByte2.toString());
                // Inserting Contacts
                Log.d("Insert: ", "Inserting ..");
                db.addUser(new User(g.getUserName(), imageInByte2));
                Intent i2 = new Intent(mainActivity.this, mainActivity.class);
                startActivity(i2);
                finish();
                break;
        }
    }

    //open gallery method
    public void callGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 0);
        intent.putExtra("aspectY", 0);
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_GALLERY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.parent:

                AlertDialog.Builder verify = new AlertDialog.Builder(this);
                verify.setTitle("Verifying access");
                verify.setMessage("56 / 8 = ?");

                final EditText response = new EditText(this);
                response.setInputType(2);
                verify.setView(response);

                verify.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton){
                        int result;
                        String r = response.getText().toString();
                        if(r.equals(""))
                            result = 0;
                        else
                            result = Integer.parseInt(r);
                        boolean allow =  false;
                        if(result == 7)
                            allow = true;
                        if(allow){
                            Intent intentP = new Intent(mainActivity.this, DataListActivity.class);
                            startActivity(intentP);
                            finish();
                        }
                        else{
                            Intent intentHere = new Intent(mainActivity.this, mainActivity.class);
                            startActivity(intentHere);
                            finish();
                        }

                    }
            });
            verify.show();
            break;
            case R.id.btnAdd:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Hello!");
                alert.setMessage("Put your name:");

                // Create EditText for entry
                final EditText input = new EditText(this);
                input.setInputType(1);
                alert.setView(input);

                // Make an "OK" button to save the name
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        name = input.getText().toString();
                        GlobalData gd = (GlobalData) getApplication();
                        gd.setUserName(name);
                        // Welcome the new user
                        Toast.makeText(getApplicationContext(), "Welcome " + name, Toast.LENGTH_LONG).show();
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.user);
                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        icon.compress(Bitmap.CompressFormat.PNG, 100, stream1);
                        byte imageInByte1[] = stream1.toByteArray();
                        db.addUser(new User(gd.getUserName(), imageInByte1));
                        Intent i2 = new Intent(mainActivity.this, mainActivity.class);
                        startActivity(i2);
                        finish();
                    }
                });

                alert.setNegativeButton("Choose photo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        name = input.getText().toString();
                        GlobalData gd = (GlobalData) getApplication();
                        gd.setUserName(name);
                        Toast.makeText(getApplicationContext(), "Welcome " + name, Toast.LENGTH_LONG).show();
                        callGallery();
                    }
                });
                alert.show();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        imageName = imageArry.get(position).getImage();
        imageId = imageArry.get(position).getID();
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Hello!");
        alert.setMessage("Do you want to delete this user :");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                db.deleteUser(new User(imageId));
                Intent intent = new Intent(mainActivity.this, mainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        alert.show();
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String userName = imageArry.get(position).getName();
        GlobalData d = (GlobalData)getApplication();
        d.setUserName(userName);
        Intent intent = new Intent(mainActivity.this, chooseLetterActivity.class);
        startActivity(intent);
        finish();
    }
}