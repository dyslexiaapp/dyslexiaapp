package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class puzzleLevel1Activity extends Activity implements View.OnClickListener{

    ImageView part1, part2, part3, part4;
    ImageView frame1, frame2, frame3, frame4;
    ImageView stars;
    ImageView nextLevel ;
    ImageView menu;
    ImageView feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_level1);

        part1     = (ImageView)findViewById(R.id.part1);
        part2     = (ImageView)findViewById(R.id.part2);
        part3     = (ImageView)findViewById(R.id.part3);
        part4     = (ImageView)findViewById(R.id.part4);
        stars     = (ImageView)findViewById(R.id.stars);
        nextLevel = (ImageView)findViewById(R.id.nextLevel);
        menu      = (ImageView) findViewById(R.id.menu);
        feedback = (ImageView)findViewById(R.id.feedback);

        ChoiceTouchListener choice = new ChoiceTouchListener();

        part1.setOnTouchListener(choice);
        part2.setOnTouchListener(choice);
        part3.setOnTouchListener(choice);
        part4.setOnTouchListener(choice);
        nextLevel.setOnClickListener(this);
        menu.setOnClickListener(this);

        frame1 = (ImageView)findViewById(R.id.frame1);
        frame2 = (ImageView)findViewById(R.id.frame2);
        frame3 = (ImageView)findViewById(R.id.frame3);
        frame4 = (ImageView)findViewById(R.id.frame4);


        ChoiceDragListener drag = new ChoiceDragListener();

        frame1.setOnDragListener(drag);
        frame2.setOnDragListener(drag);
        frame3.setOnDragListener(drag);
        frame4.setOnDragListener(drag);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.puzzle_level1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.nextLevel){
            Intent intent = new Intent(puzzleLevel1Activity.this, puzzleLevel2Activity.class);
            startActivity(intent);
            finish();
        }
        if(v.getId()==R.id.menu){
            Intent intent = new Intent(puzzleLevel1Activity.this , gameActivity.class);
            startActivity(intent);
            finish();
        }

    }

    private class ChoiceTouchListener implements View.OnTouchListener{

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //setup drag

                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);

                //start dragging the item touched
                v.startDrag(data, shadowBuilder, v, 0);
                return true;
            }
            else {
                return false;
            }
        }
    }

    private class ChoiceDragListener implements View.OnDragListener {

        int count = 0;
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {

            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    //no action necessary
                    break;
                case DragEvent.ACTION_DROP:

                    View view = (View) dragEvent.getLocalState();
                    //view dragged item is being dropped on
                    ImageView dropTarget = (ImageView) v;
                    //view being dragged and dropped
                    ImageView dropped = (ImageView) view;

                    if ((dropped.getId()== R.id.part1)&&(dropTarget.getId()==R.id.frame1)) {
                        dropped.setVisibility(View.INVISIBLE);
                        frame1.setImageResource(R.drawable.part1);
                        count++;
                    }
                    else {
                        if ((dropped.getId() == R.id.part2) && (dropTarget.getId() == R.id.frame2)) {
                            dropped.setVisibility(View.INVISIBLE);
                            frame2.setImageResource(R.drawable.part2);
                            count++;
                        }
                        else {
                            if ((dropped.getId() == R.id.part3) && (dropTarget.getId() == R.id.frame3)) {
                                dropped.setVisibility(View.INVISIBLE);
                                frame3.setImageResource(R.drawable.part3);
                                count++;
                            }
                            else {
                                if ((dropped.getId() == R.id.part4) && (dropTarget.getId() == R.id.frame4)) {
                                    dropped.setVisibility(View.INVISIBLE);
                                    frame4.setImageResource(R.drawable.part4);
                                    count++;
                                }
                                else
                                {
                                    feedback.setImageResource(R.drawable.cross);
                                    feedback.setVisibility(View.VISIBLE);
                                    feedback.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            feedback.setVisibility(View.INVISIBLE);
                                        }
                                    }, 1500);
                                }
                            }
                        }
                    }
                    if (count==4){
                        stars.setVisibility(View.VISIBLE);
                    }
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(puzzleLevel1Activity.this , gameActivity.class);
        startActivity(intent);
        finish();
    }
}

