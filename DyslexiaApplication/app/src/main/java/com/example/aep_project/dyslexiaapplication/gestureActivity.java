package com.example.aep_project.dyslexiaapplication;

import android.app.Activity;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class gestureActivity extends Activity implements GestureOverlayView.OnGesturePerformedListener , View.OnClickListener {
    int count_gestures=0;
    int count_gestures_right=0;
    public GestureLibrary mLibrary;
    String gestureID;
    int videoID;
    ImageView back;
    ImageView home;
    TextView letter;
    ImageView feedback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture);

        feedback = (ImageView)findViewById(R.id.feedback);

        Intent intent = getIntent();
        gestureID = intent.getStringExtra("gestureID");
        videoID = intent.getIntExtra("videoID", 0);
        mLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!mLibrary.load()) {
            finish();
        }

        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(this);

        back = (ImageView) findViewById(R.id.arrowBack);
        back.setOnClickListener(this);

        home = (ImageView) findViewById(R.id.home);
        home.setOnClickListener(this);

        GlobalData gd = (GlobalData)getApplication();
        letter = (TextView)findViewById(R.id.textView1);
        letter.setText(letter.getText() + " " +gd.getLetter() );
    }

    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mLibrary.recognize(gesture);

        if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
            String result = predictions.get(0).name;

            if (gestureID.equalsIgnoreCase(result)) {
                count_gestures_right++;
                count_gestures++;
                 feedback.setImageResource(R.drawable.tick);
                 feedback.setVisibility(View.VISIBLE);
                 feedback.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                 feedback.setVisibility(View.INVISIBLE);
                     }
                    }, 1500);
            } else {
                count_gestures++;
                feedback.setImageResource(R.drawable.cross);
                feedback.setVisibility(View.VISIBLE);
                feedback.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feedback.setVisibility(View.INVISIBLE);
                    }
                }, 1500);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.arrowBack:
                Intent intent = new Intent(gestureActivity.this, writeActivity.class);
                intent.putExtra("videoID", videoID);
                intent.putExtra("gestureID", gestureID);
                GlobalData gd1 = (GlobalData)getApplication();
                gd1.setGestureTrials(gd1.getGestureTrials() + count_gestures);
                gd1.setGestureRight(gd1.getGestureRight() + count_gestures_right);
                startActivity(intent);
                break;
            case R.id.home:
                Intent intent1 = new Intent(gestureActivity.this, chooseActivity.class);
                GlobalData gd = (GlobalData)getApplication();
                gd.setGestureTrials(gd.getGestureTrials() + count_gestures);
                gd.setGestureRight(gd.getGestureRight() + count_gestures_right);
                startActivity(intent1);
                break;
        }
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent(gestureActivity.this, writeActivity.class);
        intent.putExtra("videoID", videoID);
        intent.putExtra("gestureID", gestureID);
        GlobalData gd = (GlobalData)getApplication();
        gd.setGestureTrials(gd.getGestureTrials() + count_gestures);
        gd.setGestureRight(gd.getGestureRight() + count_gestures_right);

        startActivity(intent);
        finish();
    }
}